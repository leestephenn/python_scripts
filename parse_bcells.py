### This is used to find duplicates in rows of lists of spreadsheet cells ###
from sys import stdin
import re

ptrn = re.compile("B([0-9]+)")
lineno = 0
maximum = 0
minimum = 256
cells = {}
for line in stdin:
  for match in ptrn.finditer(line):
    try:
      val = match.group(1)
      num = int(val)
      if num > maximum:
        maximum = num
      if num < minimum:
        minimum = num

      if num in cells:
        cells[num].append(lineno)
      else:
        cells[num] = [lineno]
    except ValueError:
      pass
  lineno += 1

print()

for (cell, found) in cells.items():
  if len(found) > 1:
    print("Duplicate:", "B{0}".format(cell), "in", *[n + minimum for n in found])

print()

for num in range(minimum, maximum + 1):
  if num not in cells:
    print("Missing:", "B{0}".format(num))

print()

print("Min,", "max:", minimum, maximum)
