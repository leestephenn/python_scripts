# It seems this is for collecting CCLI numbers from a spreadsheet of songs sung at church
import re
import argparse
from csv import reader
from collections import Counter
from itertools import chain


def find_ccli(ccli, row):
  matches = (ccli.search(cell) for cell in row)
  return (m.group(1) for m in matches if m != None )

def find_date(row, date_pattern):
  date = ""
  for cell in row:
    m = date_pattern.search(cell) 
    if m:
      date = m.group()
      break
  return date
  

def print_numbers(reader, output):
  ccli = re.compile(r'SS\s*#\s*(\d+)')
  for row in reader:
    numbers = find_ccli(ccli, row)
    output.append(numbers)
  return numbers

parser = argparse.ArgumentParser()
parser.add_argument('files', metavar='FILE', type=str, nargs='+')

args = parser.parse_args()
output = []
for name in args.files:
  with open(name) as f:
    numbers = print_numbers(reader(f), output)
many = chain(*output)
print(Counter(many))
