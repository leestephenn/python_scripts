import networkx as nx
import sys
import itertools
import re
import os.path

### This takes as input a graph in DOT format and a text file with one label of a node in that graph on each line.
### It outputs a DOT file with a subgraph of the original graph, containing only those nodes reachable (either backwards
### or forwards) from the labels input, and the edges that reach them.

G = nx.drawing.nx_agraph.read_dot(sys.argv[1])

# Print all labels of a graph, for debugging purposes
def debug_print_labels():
  print "All labels: "
  for node, d in G.nodes_iter(data=True):
      print node
      if 'label' in d:
          print d['label']
  print ''


with sys.stdin as node_file:
    labels = node_file.readlines()
    labels = [ l.decode('utf-8').rstrip('\n\r') for l in labels ]
    print labels

    nodes = filter((lambda (n, d): d.get('label', None) in labels), G.nodes_iter(data=True))
    print len(nodes)

    result = nx.MultiDiGraph()
    node_preds = [{} for x in nodes]
    for n, _ in nodes:
        pred_edges = nx.bfs_edges(G, n, True)
        pred_edges = itertools.islice(pred_edges, 100)
        result.add_edges_from(((h, t) for (t, h) in pred_edges))
        print "Number of edges:", result.number_of_edges()

        succ_edges = nx.bfs_edges(G, n, False)
        succ_edges = itertools.islice(succ_edges, 100)
        result.add_edges_from(succ_edges)
        print "Number of edges:", result.number_of_edges()

    num_nodes = 0
    for node in result.nodes_iter():
        result.node[node] = G.node[node]
        num_nodes += 1
    for h, t in result.edges_iter():
        result.edge[h][t] = G.edge[h][t]

    filename = "reachable_" + os.path.basename(sys.argv[1])
    nx.drawing.nx_agraph.write_dot(result, filename )
