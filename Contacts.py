### This translates mobile-phone contacts from an XML format output by one program into the VCARD standard ###

from sys import argv,exit
from bs4 import BeautifulSoup
import vobject

if len(argv) > 1:
  filename = argv[1]
else:
  exit()

soup=BeautifulSoup(open(filename), 'xml')
for c in soup.find_all("contact"):
    j = vobject.vCard()
    j.add('fn')
    j.fn.value = c.find("name").text
    j.add('tel')
    typ = "cell"
    number = c.find("mobile")
    if not number:
        number = c.find("phone")
        if not number:
            continue
        typ = "voice"
    j.tel.value = number.text
    j.tel.type_param = typ
    print(j.serialize())
