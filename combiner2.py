import time
import csv
import tkinter
import re
from tkinter import ttk, N, E, S, W
import tkinter.filedialog as tkFileDialog


def test_path(path):
    print(path)
    import os
    import sys

    (basepath, fname) = os.path.split(path)
    print("directory:", basepath)
    if os.path.exists(basepath):
        print("directory exists")
    else:
        print("directory does not exist!")
        sys.exit()

    if not fname:
        print("no filename provided!")
        sys.exit()
    print("filename:", fname)
    if os.path.exists(path):
        print("filename exists")
    else:
        print("filename not found!")
        print("directory contents:")
        for fn in os.listdir(basepath):
            print(fn)


# To give default output directories for a ticker, add another line following 
#the second with the same pattern, with ticker in capital letters
# Remember to enter every \ as \\
OUTPUT_DIRS = {}
OUTPUT_DIRS["STK"] = "C:\\Documents\\"

DEFAULT_TICKERS = "fcx, a, sma"

print(OUTPUT_DIRS)
print(DEFAULT_TICKERS)

# Method that actually does all the logic, called by the gui
# the *args parameter must be there (for the gui), but is never used
def filter_tickers(*args):
    ### Get the tickers from the Gui
    ticker_str = ticker_var.get().strip()
    tickers = [tic.strip().upper() for tic in ticker_str.split(",")]

    ticker_rows = {tic: [] for tic in tickers}

    ### Open the files ####

    # Open the "Open files" dialog
    input_files = tkFileDialog.askopenfilenames(parent=root, title = 'Choose files', multiple=True)

    # Bug in Python: on linux the above returns a tuple, on Windows a string.
    # We want a tuple, which the below parses
    if not isinstance(input_files, tuple):
        # The string is formatted so that if filenames have spaces in them,
        # they are surrounded by {}: if not, left alone.
        # Get the file names with braces around them
        some_files = re.findall('\{(.*?)\}', input_files)
        #remove those file names from the string
        input_files = re.sub('\{(.*?)\}', "", input_files)
        # Get the other filenames with no spaces in them
        some_files.extend(input_files.split())
        # set input_files to the files we have collected
        input_files = some_files
        
    ### Filter the Files ###

    # An list of lists of strings, each element of which will be written as a row of the output CSV file
    output_rows = []
    # the header row
    header = []
    for file in input_files:
        # Open the CSV file and give that file handle the name csvfile
        with open(file) as csvfile:
            # Get a reader for that particular CSV file, saying that its elements are delimited by commas and that quote characters escapes things
                csv_reader = csv.reader(csvfile, delimiter = ',', quotechar = '"')
                header = csv_reader.__next__() # The header is the first row of the reader
                for line in csv_reader:
                    # For each ticker 
                    for tic in tickers:
                        if line[0] == tic:
                            ticker_rows[tic].append(line[1:])
        # The file is automatically closed when the block ends

    unsaved_tickers = [] # Collect the tickers that are not saved
    default_filename = ""
    for tic in ticker_rows:
         
        #Sorts the lists by time
        ticker_rows[tic].sort(key = lambda row: time.strptime(row[0], "%d %b %Y %H:%M"))
        # inserts the header row, less the "Symbol" label, as the first row of the output
        ticker_rows[tic].insert(0, header[1:]) 

        #Open the "Save file" dialog, saying that, if no extension is given, use .csv
        output_file = tkFileDialog.asksaveasfilename(parent=root, title = ('Save ticker %s to file' % tic), defaultextension='.csv', initialdir=OUTPUT_DIRS.setdefault(tic), initialfile=default_filename)
        default_filename=output_file

        # Open the file just selected, if it is not the empty string
        if output_file:
            with open(output_file, 'wt') as file:
                csv_writer = csv.writer(file, delimiter = ',', quotechar = '"', quoting = csv.QUOTE_MINIMAL)
                # Write all the elements of the output rows to the file 
                csv_writer.writerows(ticker_rows[tic])
        else: #if the file is the empty string
            unsaved_tickers.append(tic)
    
    ticker_var.set(",".join(unsaved_tickers))



# A window to base the file dialogs on
root = tkinter.Tk()
# The panel everything is on
mainframe = ttk.Frame(root, padding = "3 3 12 12")
# This frame is in the first column and first row of the window, 
# and sticks to every side
mainframe.grid(column = 0, row =0, sticky = (N, W, E, S))
# have the main frame expand to fill the entire window
mainframe.columnconfigure(0, weight = 1), mainframe.rowconfigure(0, weight=1)
# A simple label for the ticker entry, in the 0th column and row of the frame
ttk.Label(mainframe, text = "Tickers:").grid(column=0,row=0)
# A gui variable for the ticker: this is how you access the contents of a textbox
ticker_var = tkinter.StringVar()
#Set the default value of the textbox
ticker_var.set(DEFAULT_TICKERS)
# Make an entry (textbox) to enter the ticker in, with the variable just created
ticker_entry = ttk.Entry(mainframe, width=32, textvariable=ticker_var)
# Place that entry in the 0th column and 1st row of the frame
ticker_entry.grid(column=0,row=1, sticky = (W, E))
# Put the keyboard focus on the entry
ticker_entry.focus()
# Make hitting "Enter" anywhere in the window call the filter_ticket method
root.bind('<Return>', filter_tickers)
# Make a button, with text "Filter" which also calls filter_tickers when pressed
button = ttk.Button(mainframe, text = "Filter", command=filter_tickers)
# Place it in the 0th column, 2nd row
button.grid(column=0,row =2, sticky = (W,E))
# Start the gui
root.mainloop()

