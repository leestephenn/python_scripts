# This script calculates the average time and average memory usage of multiple runs of a program
# It processes the time and memory usage of each run as output by /usr/bin/time -v
BEGIN {
  FS=":"
}
{
  if ($1 ~ /User time/) {
    tsum += $2;
    tn++
  }
}
{
  if ($1 ~ /Maximum resident set/) {
    msum += $2;
    mn++
  }
}
END {
  print "Average time (s): "tsum/tn; print "Average memory usage (kB): "msum/mn
}
